for $l in
distinct-values(
for $d in collection("izbor_pos_aldt")//*:word[@postag="r--------"]
let $lemma := $d/@form/string()
return $lemma
)
order by $l collation "?lang=el"
return $l