declare function local:pers_morph($w){
  let $call := "http://morph.perseids.org/analysis/word?lang=grc&amp;engine=morpheusgrc&amp;word=" || web:encode-url($w)
  return 
  http:send-request(<http:request method='get'>
  <http:header name="Accept" value="application/xml"/>
  </http:request>, $call)
};
for $r in db:open("greeklatincore", "greek_core_list_2.xml")//*:record
let $lp := $r/*:LemmaPers/string()
(: let $persMatch := collection("greekCoreInPerseusLexEnt")//*:entry[*:dict/*:hdwd/string()=$lp] :)
where $r/*:lexent[not(@uri)]
return local:pers_morph($lp)//*:RDF//*:Body