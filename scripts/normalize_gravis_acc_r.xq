declare variable $dmap := map {
  "ἂν": "ἄν",
"ἀντ᾽": "ἀντί",
"ἀπὸ": "ἀπό",
"διὰ": "διά",
"διʼ": "διά",
"δʼ": "διά",
"ἔν": "ἐν",
"ἐπὶ": "ἐπί",
"ἐπʼ": "ἐπί",
"ἔς": "ἐς",
"ἐφʼ": "ἐπί",
"καθ᾽": "κατά",
"κατὰ": "κατά",
"μετὰ": "μετά",
"μετʼ": "μετά",
"παρὰ": "παρά",
"παρʼ": "παρά",
"περὶ": "περί",
"πλὴν": "πλήν",
"πρὸ": "πρό",
"πρὸς": "πρός",
"σὺν": "σύν",
"ὑπὸ": "ὑπό"
};
for $m in map:keys($dmap)
for $d in collection("izbor_pos_aldt")//*:word[@postag="r--------" and @lemma=$m]
let $newlemma := map:get($dmap, $m)
let $attr := $d/@lemma
return replace value of node $attr with $newlemma