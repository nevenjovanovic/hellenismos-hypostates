for $w in db:open("PersPOS")//*:word
let $lemma := $w/@lemma/string()
group by $lemma
(: order by count($w) descending , $lemma collation "?lang=el" :)
order by $lemma collation "?lang=el"
return element w { attribute count { count($w) } , element lem { $lemma } 
(: , for $singul in $w return element p { db:path($singul) } :)
 }