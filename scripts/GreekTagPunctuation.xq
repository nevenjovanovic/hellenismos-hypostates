declare function local:tokenize-ws($string){
  (: tokenize on whitespace :)
  let $tokens := tokenize($string, "\s")
  (: analyze-string :)
  for $t in $tokens
  for $punc in analyze-string($t, "[.,;;()·’‘—„”“]")//*
  return  $punc
};
declare function local:transform-analyzes($matches){
  (: get result of analyze-string by Greek punctuation :)
  (: rename matches and non-matches :)
  for $m in $matches/*
  return switch ($m)
  case $m[name()="fn:match"] return element pc { $m/string() }
  default return element w { $m/string() }
};

(: read the db :)
let $matches := element result {
let $string := "τὴν ποίησιν· τοῖς δὲ περὶ τοὺς λόγους οὐδὲν ἔξεστιν τῶν τοιούτων, ἀλλ' ἀποτόμως καὶ τῶν ὀνομάτων τοῖς πολιτικοῖς μόνον καὶ τῶν ἐνθυμημάτων τοῖς περὶ αὐτὰς τὰς πράξεις ἀναγκαῖόν ἐστιν χρῆσθαι. Πρὸς δὲ τούτοις οἱ μὲν μετὰ μέτρων καὶ ῥυθμῶν ἅπαντα ποιοῦσιν, οἱ δ' οὐδενὸς τούτων κοινωνοῦσιν· ἃ τοσαύτην ἔχει χάριν ὥστ', ἂν καὶ τῇ λέξει καὶ τοῖς ἐνθυμήμασιν ἔχῃ κακῶς, ὅμως αὐταῖς ταῖς εὐρυθμίαις καὶ ταῖς συμμετρίαις ψυχαγωγοῦσιν τοὺς ἀκούοντας."
(: tokenize on whitespace :)
return local:tokenize-ws($string)
}
return local:transform-analyzes($matches)