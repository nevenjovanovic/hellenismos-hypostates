let $words := let $file := file:read-text("/home/neven/Repos/grc_lektire/rijeci/GrcMorfRijeciNauciti.txt")
let $csv := csv:parse($file, map { 'header': true(), 'separator': 'semicolon' })
return $csv
for $word in $words//record/riječ/string()
for $w in tokenize($word, " ")
let $pers := db:open("greeklatincore", "greek_core_list_2.xml")//*:record[*:LemmaPers/string()=$w]
return if ($pers) then ($w , $pers/*:lexent/@uri/string()) else ()