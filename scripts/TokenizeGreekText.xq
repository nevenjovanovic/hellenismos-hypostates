
(: for $db in ("grc_db_txt_p1", "grc_db_txt_p2"):)
for $t in collection("grc_db_txt_p1")//*:line[string()]
let $id := db:path($t)
return element p {
  attribute id { $id },
let $tokens := tokenize($t, "[\s\r\n.,;;()·’‘—„”“]+")
for $t in $tokens
return element w { $t }
}