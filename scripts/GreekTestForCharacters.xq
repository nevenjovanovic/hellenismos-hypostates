declare function local:tokenize-ws($string){
  (: tokenize on whitespace :)
  let $tokens := tokenize($string, "\s")
  (: analyze-string :)
  for $t in $tokens
  return analyze-string($t, "[^\p{IsGreek},\p{IsGreekExtended}]+")//*:match/string()
};
let $matches := 
let $strings := (collection("grc_db_txt_p1"),collection("grc_db_txt_p2"))//*:line[string()]
for $string in $strings
let $id := db:path($string)
(: tokenize on whitespace :)
return 
  local:tokenize-ws($string/string())

return distinct-values($matches)