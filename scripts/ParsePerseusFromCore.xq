declare function local:pers_morph($w){
  let $call := "http://morph.perseids.org/analysis/word?lang=grc&amp;engine=morpheusgrc&amp;word=" || web:encode-url($w)
  return 
  http:send-request(<http:request method='get'>
  <http:header name="Accept" value="application/xml"/>
  </http:request>, $call)
};
for $l in db:open("greeklatincore", "greek_core_list_2.xml")//record
let $parse := local:pers_morph($l/LemmaPers/string())
return insert node $parse//*:entry/*:dict/*:hdwd after $l/LemmaPers