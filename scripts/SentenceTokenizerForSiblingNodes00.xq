let $set := <set>
<w>A</w>
<w>C</w>
<pc>.</pc>
<w>B</w>
<pc>,</pc>
<w>D</w>
<pc>?</pc>
</set>
(: test for sentence end chars :)
for $a in $set/*
where matches($a/string(), "[.?]")
let $sentence := if (not($a[preceding-sibling::pc[matches(.,  "[.?]")]])) then  $a/preceding-sibling::*
else $a/preceding-sibling::*[preceding-sibling::pc[matches(.,  "[.?]")]]
return element sentence { $sentence , $a }
