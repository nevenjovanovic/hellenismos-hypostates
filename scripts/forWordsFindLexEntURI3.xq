declare variable $words := ( let $file := file:read-text("/home/neven/Repos/grc_lektire/rijeci/GrcMorfRijeciNauciti.txt")
let $csv := csv:parse($file, map { 'header': true(), 'separator': 'semicolon' })
return $csv );
let $wordlist := element wordlist {
  attribute ana { "zadano"},
for $word in $words//record
let $w := tokenize($word/riječ/string(), " ")[1]
let $pers := db:open("greeklatincore", "greek_core_list_2.xml")//*:record[*:LemmaPers/string()=$w]
return element record { $word/* , 
$pers/*:FREQUENCY-RANK ,
$pers/*:LemmaPers ,
$pers/*:lexent }
}
return file:write("/home/neven/Repos/grc_lektire/rijeci/GrcMorfRijeciNauciti.xml", $wordlist)