(: Create DB from PerseusDL Github repo, Greek :)
let $path := '/home/neven/Repos/canonical-greekLit/data/'
let $files := file:list($path, true())
for $f in $files
where matches($f, 'grc[1-5].xml$')
return db:add('grc_db', $path || $f)