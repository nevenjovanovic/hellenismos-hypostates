(: Create DB from a CSV of the Greek reading list :)
let $readings :=
let $path := '/home/neven/Repos/grc_lektire/csv/lektire_tekstovi_popis.csv'
let $f := file:read-text($path)
let $csv := csv:parse($f, map { 'separator': 'semicolon' })
for $cts in $csv//*:record/*:entry[4]
return substring-after(data($cts), 'greekLit:')
for $r in $readings
let $n := $r || ".perseus-grc1.xml"
let $o := $r || ".perseus-grc2.xml"
let $path2 := "/home/neven/Repos/grc_lektire/data/"
return if (db:exists('grc_db', $n)) then ()
else if (db:exists('grc_db', $o)) then ()
else for $d in db:list("grc_db_1kg")
where starts-with($d, $r)
return file:write($path2 || $d , db:open('grc_db_1kg', $d))