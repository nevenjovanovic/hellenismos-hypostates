let $set := collection("grc_db_w_pc")
(: test for sentence end chars :)
for $a in $set/texts/pc
where matches($a/string(), "[.;;]+")
return insert node element sb {} after $a