for $g in db:open("greeklatincore", "greek_core_list_2.xml")//*:record
let $perslem := $g/*:LemmaPers/string()
let $pers := collection("greekCoreInPerseusLexEnt")//*:entry[*:dict/*:hdwd/string()=$perslem]
let $lexent := $pers[1]/@uri
return insert node element lexent { $lexent } into $g