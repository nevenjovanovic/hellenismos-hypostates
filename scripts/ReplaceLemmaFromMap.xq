declare variable $words := map {
  "ΚΛΕΠΤΗΣ": "κλέπτης", "μεγάλου" : "μέγας", "μεγάλῳ" : "μέγας", "ΜΗΤΗΡ" : "μήτηρ", "ΠΑΙΣ" : "παῖς", "ΠΙΘΗΚΟΣ" : "πίθηκος", "ΣΑΤΥΡΟΣ" : "σάτυρος", "ΤΡΑΓΟΣ": "τράγος"
};
declare %updating function local:addlemma($key , $value){
  for $s in collection("grc_div_s_wpc")//w
where $s/string()=$key
return replace value of node $s/@lemma with $value
};
declare function local:showlemma($key , $value){
  for $s in collection("grc_div_s_wpc")//w
where $s/string()=$key
return ( $s , $value )
};
(: map:for-each (
 $words ,
 function($key , $value){ local:addlemma($key, $value)}
) :)
for $k in map:keys($words)
let $v := map:get($words, $k)
return local:showlemma( $k , $v )