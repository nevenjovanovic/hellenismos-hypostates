(: Create DB of files morphologically annotated in Perseids :)
(: folder pos_perseids :)
(: Grčka morfologija 1 :)
let $file := for $f in file:list('/home/neven/Repos/grc_lektire/pos_perseids/', false(), '*.xml') return '/home/neven/Repos/grc_lektire/pos_perseids/' || $f
return db:create('PersPOS', $file , (), map { 'intparse': true(), 'updindex': true(), 'autooptimize': true() } )