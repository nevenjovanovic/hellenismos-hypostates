for $v in ("r--------", "c--------")
for $d in collection("izbor_pos_aldt")//*:word[@postag=$v]
let $lemma := $d/@form/string()
let $lemma_attr := $d/@lemma
return replace value of node $lemma_attr with $lemma