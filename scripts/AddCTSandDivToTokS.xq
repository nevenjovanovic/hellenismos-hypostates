for $s in collection("grc_tok_sent")//s
let $text := $s/w[1]/@id/string()
group by $text
return element div {
  attribute id { $text },
  $s
}
