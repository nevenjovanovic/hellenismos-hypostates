for $d in collection("grc_div_s_wpc")//div/s
let $pos := count($d/preceding-sibling::s) + 1
let $id := substring-before($d/../@id, ".txt")
return insert node attribute id { $id || "." || $pos } into $d