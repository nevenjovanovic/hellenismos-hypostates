for $t in db:open("izbor_pos_ud")//*:text
let $name := db:path($t)
let $did := $t/@text-cts
let $ss :=
for $s in $t/*:s
let $id := $s/@n
let $w := for $t in $s/*:t
return element word { 
attribute id { $t/@n },
attribute form { $t/text() },
attribute lemma {  },
attribute postag { $t/@o },
attribute relation { },
attribute head {  }
}
return element sentence {
  attribute id { $id } ,
  attribute document_id { $did },
  attribute subdoc {  },
  attribute span {  },
  $w
}
let $tb := element treebank {
  attribute xml:lang {"grc"},
  attribute format {"aldt"},
  attribute direction {"ltr"},
  attribute version {"1.5"},
  $ss
}
return db:add("izbor_pos_aldt", $tb, $name)