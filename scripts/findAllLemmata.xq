for $l in distinct-values(
for $w in collection("pos_perseids")//*:word
let $lemma := $w/@lemma/string()
where not($lemma="")
return $lemma )
order by $l collation "?lang=el"
return $l