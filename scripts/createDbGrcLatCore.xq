(: Create DB of Greek and Latin Core :)
(: folder rijeci/GrcLatCore :)
(: Grčka morfologija 1 :)
let $file := for $f in file:list('/home/neven/Repos/grc_lektire/rijeci/GrcLatCore/', false(), '*.xml') return '/home/neven/Repos/grc_lektire/rijeci/GrcLatCore/' || $f
return db:create('greeklatincore', $file , (), map { 'intparse': true(), 'updindex': true(), 'autooptimize': true(), 'ftindex': true() } )