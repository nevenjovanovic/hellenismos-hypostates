declare function local:tokenize-ws($string){
  (: tokenize on whitespace :)
  let $tokens := tokenize($string, "\s")
  (: analyze-string :)
  for $t in $tokens
  for $punc in analyze-string($t, "[.,;;()·’‘—„”“]")//*
  return  $punc
};
declare function local:transform-analyzes($matches){
  (: get result of analyze-string by Greek punctuation :)
  (: rename matches and non-matches :)
  element texts {
  for $m in $matches/*
  let $id := $m/../@id
  return switch ($m)
  case $m[name()="fn:match"] return element pc {
    $id , $m/string() }
  default return element w { $id , $m/string() }
}
};

(: read the db :)
let $matches := 
let $strings := (collection("grc_db_txt_p1"),collection("grc_db_txt_p2"),collection("grc_db_txt_p3"))//*:line[string()]
for $string in $strings
let $id := db:path($string)
(: tokenize on whitespace :)
return element result {
  attribute id { $id },
  local:tokenize-ws($string/string())
}
return db:create("grc_db_w_pc", local:transform-analyzes($matches), "grctxt.xml", map { 'ftindex': true(), 'intparse': true(), 'updindex': true(), 'autooptimize': true(), 'language': 'el', 'diacritics': 'true' })