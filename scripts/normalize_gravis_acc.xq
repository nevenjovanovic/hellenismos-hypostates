declare variable $dmap := map {
  "αἰεὶ": "αἰεί",
"ἀλλʼ": "ἀλλά",
"ἂν": "ἄν",
"ἀπʼ": "ἀπό",
"γὰρ": "γάρ",
"δὲ": "δέ",
"δὴ": "δή",
"διʼ": "διά",
"δʼ": "δέ",
"ἐπʼ": "ἐπί",
"καὶ": "καί",
"μάλιστʼ": "μάλιστα",
"μὲν": "μέν",
"μετ᾽": "μετά",
"μὴ": "μή",
"μηδὲ": "μηδέ",
"οἷʼ": "οἶος",
"οὐδʼ": "οὐδὲ",
"τέ": "τε"
};
for $m in map:keys($dmap)
for $d in collection("izbor_pos_aldt")//*:word[@postag="d--------" and @lemma=$m]
let $newlemma := map:get($dmap, $m)
let $attr := $d/@lemma
return replace value of node $attr with $newlemma