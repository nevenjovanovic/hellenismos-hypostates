let $words := let $file := file:read-text("/home/neven/Repos/grc_lektire/rijeci/GrcMorfRijeciNauciti.txt")
let $csv := csv:parse($file, map { 'header': true(), 'separator': 'semicolon' })
return $csv
for $word in $words//record/riječ/string()
for $w in tokenize($word, " ")
let $pers := collection("greekCoreInPerseusLexEnt")//*:entry[*:dict/*:hdwd/string()=$w]
return if ($pers) then ($pers/*:dict/*:hdwd/string() , $pers/@uri/string() ) else()