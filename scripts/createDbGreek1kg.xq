(: Create DB from PerseusDL Github repo, Greek :)
let $path := '/home/neven/Repos/First1KGreek/data/'
let $files := file:list($path, true())
for $f in $files
where matches($f, 'grc[1-5].xml$')
return db:add('grc_db_1kg', $path || $f)