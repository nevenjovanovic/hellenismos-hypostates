declare function local:pers_morph($w){
  let $call := "http://morph.perseids.org/analysis/word?lang=grc&amp;engine=morpheusgrc&amp;word=" || web:encode-url($w)
  return 
  http:send-request(<http:request method='get'>
  <http:header name="Accept" value="application/xml"/>
  </http:request>, $call)
};
let $lp := ("μεγάλου")
let $parse := local:pers_morph($lp)
return $parse