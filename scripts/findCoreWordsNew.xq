for $l in distinct-values(
for $w in db:open("PersPOS", "tlg0003.tlg001.ffzg-pos6.8.1.xml")//*:word
let $lemma := $w/@lemma/string()
where db:open("greeklatincore", "greek_core_list_2.xml")//*:record[*:Lemma/string()=$lemma]

return if ( not(db:open("GrcMorf1Rijeci", "GrcMorfRijeciNauciti.xml")//*:LemmaPers/string()=$lemma )) then $lemma else ()
)
let $core := db:open("greeklatincore", "greek_core_list_2.xml")//*:record[*:LemmaPers/string()=$l]
let $grc := $core/*:KEYWORD/string()
let $rank := $core/*:FREQUENCY-RANK/string()
let $hr := $core/*:DefHR/string()
(: order by $l collation "?lang=el" :)
order by number($rank)
return ( $grc , $hr,  $rank)