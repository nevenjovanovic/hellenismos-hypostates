for $t in collection("grc_db_txt_p1")//*:line
let $tokens := tokenize($t, "[\s\r\n.,;;()·’‘—„”“]+")
for $t in $tokens
order by $t collation "?lang=el"
return $t