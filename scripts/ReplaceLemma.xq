declare variable $words := map {
  "ΚΛΕΠΤΗΣ": "Sun", "μεγάλου" : "Mon", "μεγάλῳ" : "Tue", "ΜΗΤΗΡ" : "Wed", "ΠΑΙΣ" : "Thu", "ΠΙΘΗΚΟΣ" : "Fri", "ΣΑΤΥΡΟΣ" : "σάτυρος", "ΤΡΑΓΟΣ": "τράγος"
};
let $string := "καλὰ"
let $lemma := "καλός"
for $s in collection("grc_div_s_wpc")//w
where $s/string()=$string
return replace value of node $s/@lemma with $lemma
(: return $s/.. :)