declare function local:pers_morph($w){
  let $call := "http://morph.perseids.org/analysis/word?lang=grc&amp;engine=morpheusgrc&amp;word=" || web:encode-url($w)
  return 
  http:send-request(<http:request method='get'>
  <http:header name="Accept" value="application/xml"/>
  </http:request>, $call)
};

let $lp := collection("grc_div_s_wpc")//div/s/w[not(@lemma)]
for $l in $lp
let $string := $l/string()
let $parse := local:pers_morph($string)
let $lemma := attribute lemma { $parse//*:entry/*:dict/*:hdwd/string() }
return insert node $lemma into $l