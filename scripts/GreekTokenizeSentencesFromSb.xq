let $text := <text>
<sb/>
  <w id="tlg0062.tlg021.opp-grc4:25.3.txt">Ἐπακούων</w>
  <w id="tlg0062.tlg021.opp-grc4:25.3.txt">δὲ</w>
  <w id="tlg0062.tlg021.opp-grc4:25.3.txt">ὁ</w>
  <w id="tlg0062.tlg021.opp-grc4:25.3.txt">Ζεὺς</w>
  <w id="tlg0062.tlg021.opp-grc4:25.3.txt">καὶ</w>
  <w id="tlg0062.tlg021.opp-grc4:25.3.txt">τὴν</w>
  <w id="tlg0062.tlg021.opp-grc4:25.3.txt">εὐχὴν</w>
  <w id="tlg0062.tlg021.opp-grc4:25.3.txt">ἑκάστην</w>
  <w id="tlg0062.tlg021.opp-grc4:25.3.txt">ἀκριβῶς</w>
  <w id="tlg0062.tlg021.opp-grc4:25.3.txt">ἐξετάζων</w>
  <w id="tlg0062.tlg021.opp-grc4:25.3.txt">οὐ</w>
  <w id="tlg0062.tlg021.opp-grc4:25.3.txt">πάντα</w>
  <w id="tlg0062.tlg021.opp-grc4:25.3.txt">ὑπισχνεῖτο</w>
  <pc id="tlg0062.tlg021.opp-grc4:25.3.txt">,</pc>
  <w id="tlg0062.tlg021.opp-grc4:25.3.txt">ἀλλ'</w>
  <w id="tlg0062.tlg021.opp-grc4:25.3.txt">ἕτερον</w>
  <w id="tlg0062.tlg021.opp-grc4:25.3.txt">μὲν</w>
  <w id="tlg0062.tlg021.opp-grc4:25.3.txt">ἔδωκε</w>
  <w id="tlg0062.tlg021.opp-grc4:25.3.txt">πατήρ</w>
  <pc id="tlg0062.tlg021.opp-grc4:25.3.txt">,</pc>
  <w id="tlg0062.tlg021.opp-grc4:25.3.txt">ἕτερον</w>
  <w id="tlg0062.tlg021.opp-grc4:25.3.txt">δ'</w>
  <w id="tlg0062.tlg021.opp-grc4:25.3.txt">ἀνένευσε</w>
  <pc id="tlg0062.tlg021.opp-grc4:25.3.txt">·</pc>
  <w id="tlg0062.tlg021.opp-grc4:25.3.txt">τὰς</w>
  <w id="tlg0062.tlg021.opp-grc4:25.3.txt">μὲν</w>
  <w id="tlg0062.tlg021.opp-grc4:25.3.txt">γὰρ</w>
  <w id="tlg0062.tlg021.opp-grc4:25.3.txt">δικαίας</w>
  <w id="tlg0062.tlg021.opp-grc4:25.3.txt">τῶν</w>
  <w id="tlg0062.tlg021.opp-grc4:25.3.txt">εὐχῶν</w>
  <w id="tlg0062.tlg021.opp-grc4:25.3.txt">προσίετο</w>
  <w id="tlg0062.tlg021.opp-grc4:25.3.txt">ἄνω</w>
  <w id="tlg0062.tlg021.opp-grc4:25.3.txt">διὰ</w>
  <w id="tlg0062.tlg021.opp-grc4:25.3.txt">τοῦ</w>
  <w id="tlg0062.tlg021.opp-grc4:25.3.txt">στομίου</w>
  <w id="tlg0062.tlg021.opp-grc4:25.3.txt">καὶ</w>
  <w id="tlg0062.tlg021.opp-grc4:25.3.txt">ἐπὶ</w>
  <w id="tlg0062.tlg021.opp-grc4:25.3.txt">τὰ</w>
  <w id="tlg0062.tlg021.opp-grc4:25.3.txt">δεξιὰ</w>
  <w id="tlg0062.tlg021.opp-grc4:25.3.txt">κατετίθει</w>
  <w id="tlg0062.tlg021.opp-grc4:25.3.txt">φέρων</w>
  <pc id="tlg0062.tlg021.opp-grc4:25.3.txt">,</pc>
  <w id="tlg0062.tlg021.opp-grc4:25.3.txt">τὰς</w>
  <w id="tlg0062.tlg021.opp-grc4:25.3.txt">δὲ</w>
  <w id="tlg0062.tlg021.opp-grc4:25.3.txt">ἀνοσίους</w>
  <w id="tlg0062.tlg021.opp-grc4:25.3.txt">ἀπράκτους</w>
  <w id="tlg0062.tlg021.opp-grc4:25.3.txt">αὖθις</w>
  <w id="tlg0062.tlg021.opp-grc4:25.3.txt">ἀπέπεμπεν</w>
  <w id="tlg0062.tlg021.opp-grc4:25.3.txt">ἀποφυσῶν</w>
  <w id="tlg0062.tlg021.opp-grc4:25.3.txt">κάτω</w>
  <pc id="tlg0062.tlg021.opp-grc4:25.3.txt">,</pc>
  <w id="tlg0062.tlg021.opp-grc4:25.3.txt">ἵνα</w>
  <w id="tlg0062.tlg021.opp-grc4:25.3.txt">μηδὲ</w>
  <w id="tlg0062.tlg021.opp-grc4:25.3.txt">πλησίον</w>
  <w id="tlg0062.tlg021.opp-grc4:25.3.txt">γένοιντο</w>
  <w id="tlg0062.tlg021.opp-grc4:25.3.txt">τοῦ</w>
  <w id="tlg0062.tlg021.opp-grc4:25.3.txt">οὐρανοῦ</w>
  <pc id="tlg0062.tlg021.opp-grc4:25.3.txt">.</pc>
  <sb/>
  <w id="tlg0003.tlg001:1.18.txt">Μετὰ</w>
  <w id="tlg0003.tlg001:1.18.txt">δὲ</w>
  <w id="tlg0003.tlg001:1.18.txt">τὴν</w>
  <w id="tlg0003.tlg001:1.18.txt">τῶν</w>
  <w id="tlg0003.tlg001:1.18.txt">τυράννων</w>
  <w id="tlg0003.tlg001:1.18.txt">κατάλυσιν</w>
  <w id="tlg0003.tlg001:1.18.txt">ἐκ</w>
  <w id="tlg0003.tlg001:1.18.txt">τῆς</w>
  <w id="tlg0003.tlg001:1.18.txt">῾Ελλάδος</w>
  <w id="tlg0003.tlg001:1.18.txt">οὐ</w>
  <w id="tlg0003.tlg001:1.18.txt">πολλοῖς</w>
  <w id="tlg0003.tlg001:1.18.txt">ἔτεσιν</w>
  <w id="tlg0003.tlg001:1.18.txt">ὕστερον</w>
  <w id="tlg0003.tlg001:1.18.txt">καὶ</w>
  <w id="tlg0003.tlg001:1.18.txt">ἡ</w>
  <w id="tlg0003.tlg001:1.18.txt">ἐν</w>
  <w id="tlg0003.tlg001:1.18.txt">Μαραθῶνι</w>
  <w id="tlg0003.tlg001:1.18.txt">μάχη</w>
  <w id="tlg0003.tlg001:1.18.txt">Μήδων</w>
  <w id="tlg0003.tlg001:1.18.txt">πρὸς</w>
  <w id="tlg0003.tlg001:1.18.txt">Ἀθηναίους</w>
  <w id="tlg0003.tlg001:1.18.txt">ἐγένετο</w>
  <pc id="tlg0003.tlg001:1.18.txt">.</pc>
  <sb/>
  <w id="tlg0003.tlg001:1.18.txt">δεκάτῳ</w>
  <w id="tlg0003.tlg001:1.18.txt">δὲ</w>
  <w id="tlg0003.tlg001:1.18.txt">ἔτει</w>
  <w id="tlg0003.tlg001:1.18.txt">μετ'</w>
  <w id="tlg0003.tlg001:1.18.txt">αὐτὴν</w>
  <w id="tlg0003.tlg001:1.18.txt">αὖθις</w>
  <w id="tlg0003.tlg001:1.18.txt">ὁ</w>
  <w id="tlg0003.tlg001:1.18.txt">βάρβαρος</w>
  <w id="tlg0003.tlg001:1.18.txt">τῷ</w>
  <w id="tlg0003.tlg001:1.18.txt">μεγάλῳ</w>
  <w id="tlg0003.tlg001:1.18.txt">στόλῳ</w>
  <w id="tlg0003.tlg001:1.18.txt">ἐπὶ</w>
  <w id="tlg0003.tlg001:1.18.txt">τὴν</w>
  <w id="tlg0003.tlg001:1.18.txt">῾Ελλάδα</w>
  <w id="tlg0003.tlg001:1.18.txt">δουλωσόμενος</w>
  <w id="tlg0003.tlg001:1.18.txt">ἦλθεν</w>
  <pc id="tlg0003.tlg001:1.18.txt">.</pc>
  <sb/>
  <w id="tlg0003.tlg001:1.18.txt">καὶ</w>
  <w id="tlg0003.tlg001:1.18.txt">μεγάλου</w>
  <w id="tlg0003.tlg001:1.18.txt">κινδύνου</w>
  <w id="tlg0003.tlg001:1.18.txt">ἐπικρεμασθέντος</w>
  <w id="tlg0003.tlg001:1.18.txt">οἵ</w>
  <w id="tlg0003.tlg001:1.18.txt">τε</w>
  <w id="tlg0003.tlg001:1.18.txt">Λακεδαιμόνιοι</w>
  <w id="tlg0003.tlg001:1.18.txt">τῶν</w>
  <w id="tlg0003.tlg001:1.18.txt">ξυμπολεμησάντων</w>
  <w id="tlg0003.tlg001:1.18.txt">῾Ελλήνων</w>
  <w id="tlg0003.tlg001:1.18.txt">ἡγήσαντο</w>
  <w id="tlg0003.tlg001:1.18.txt">δυνάμει</w>
  <w id="tlg0003.tlg001:1.18.txt">προύχοντες</w>
  <pc id="tlg0003.tlg001:1.18.txt">,</pc>
  <w id="tlg0003.tlg001:1.18.txt">καὶ</w>
  <w id="tlg0003.tlg001:1.18.txt">οἱ</w>
  <w id="tlg0003.tlg001:1.18.txt">Ἀθηναῖοι</w>
  <w id="tlg0003.tlg001:1.18.txt">ἐπιόντων</w>
  <w id="tlg0003.tlg001:1.18.txt">τῶν</w>
  <w id="tlg0003.tlg001:1.18.txt">Μήδων</w>
  <w id="tlg0003.tlg001:1.18.txt">διανοηθέντες</w>
  <w id="tlg0003.tlg001:1.18.txt">ἐκλιπεῖν</w>
  <w id="tlg0003.tlg001:1.18.txt">τὴν</w>
  <w id="tlg0003.tlg001:1.18.txt">πόλιν</w>
  <w id="tlg0003.tlg001:1.18.txt">καὶ</w>
  <w id="tlg0003.tlg001:1.18.txt">ἀνασκευασάμενοι</w>
  <w id="tlg0003.tlg001:1.18.txt">ἐς</w>
  <w id="tlg0003.tlg001:1.18.txt">τὰς</w>
  <w id="tlg0003.tlg001:1.18.txt">ναῦς</w>
  <w id="tlg0003.tlg001:1.18.txt">ἐσβάντες</w>
  <w id="tlg0003.tlg001:1.18.txt">ναυτικοὶ</w>
  <w id="tlg0003.tlg001:1.18.txt">ἐγένοντο</w>
  <pc id="tlg0003.tlg001:1.18.txt">.</pc>
  <sb/>
  <w id="tlg0003.tlg001:1.18.txt">κοινῇ</w>
  <w id="tlg0003.tlg001:1.18.txt">τε</w>
  <w id="tlg0003.tlg001:1.18.txt">ἀπωσάμενοι</w>
  <w id="tlg0003.tlg001:1.18.txt">τὸν</w>
  <w id="tlg0003.tlg001:1.18.txt">βάρβαρον</w>
  <pc id="tlg0003.tlg001:1.18.txt">,</pc>
  <w id="tlg0003.tlg001:1.18.txt">ὕστερον</w>
  <w id="tlg0003.tlg001:1.18.txt">οὐ</w>
  <w id="tlg0003.tlg001:1.18.txt">πολλῷ</w>
  <w id="tlg0003.tlg001:1.18.txt">διεκρίθησαν</w>
  <w id="tlg0003.tlg001:1.18.txt">πρός</w>
  <w id="tlg0003.tlg001:1.18.txt">τε</w>
  <w id="tlg0003.tlg001:1.18.txt">Ἀθηναίους</w>
  <w id="tlg0003.tlg001:1.18.txt">καὶ</w>
  <w id="tlg0003.tlg001:1.18.txt">Λακεδαιμονίους</w>
  <w id="tlg0003.tlg001:1.18.txt">οἵ</w>
  <w id="tlg0003.tlg001:1.18.txt">τε</w>
  <w id="tlg0003.tlg001:1.18.txt">ἀποστάντες</w>
  <w id="tlg0003.tlg001:1.18.txt">βασιλέως</w>
  <w id="tlg0003.tlg001:1.18.txt">῞Ελληνες</w>
  <w id="tlg0003.tlg001:1.18.txt">καὶ</w>
  <w id="tlg0003.tlg001:1.18.txt">οἱ</w>
  <w id="tlg0003.tlg001:1.18.txt">ξυμπολεμήσαντες</w>
  <pc id="tlg0003.tlg001:1.18.txt">.</pc>
  <sb/>
  <w id="tlg0003.tlg001:1.18.txt">δυνάμει</w>
  <w id="tlg0003.tlg001:1.18.txt">γὰρ</w>
  <w id="tlg0003.tlg001:1.18.txt">ταῦτα</w>
  <w id="tlg0003.tlg001:1.18.txt">μέγιστα</w>
  <w id="tlg0003.tlg001:1.18.txt">διεφάνη</w>
  <pc id="tlg0003.tlg001:1.18.txt">·</pc>
  <w id="tlg0003.tlg001:1.18.txt">ἴσχυον</w>
  <w id="tlg0003.tlg001:1.18.txt">γὰρ</w>
  <w id="tlg0003.tlg001:1.18.txt">οἱ</w>
  <w id="tlg0003.tlg001:1.18.txt">μὲν</w>
  <w id="tlg0003.tlg001:1.18.txt">κατὰ</w>
  <w id="tlg0003.tlg001:1.18.txt">γῆν</w>
  <pc id="tlg0003.tlg001:1.18.txt">,</pc>
  <w id="tlg0003.tlg001:1.18.txt">οἱ</w>
  <w id="tlg0003.tlg001:1.18.txt">δὲ</w>
  <w id="tlg0003.tlg001:1.18.txt">ναυσίν</w>
  <pc id="tlg0003.tlg001:1.18.txt">.</pc>
  <sb/>
</text>
for $begin in $text//*:sb
let $n := ("w", "pc")
let $end := $begin/following-sibling::sb[1]
return element s { $begin/following-sibling::*[name()=$n and count(.|$end/preceding-sibling::*[name()=$n]) = count($end/preceding-sibling::*[name()=$n])]
 }