riječ ; prijevod ; lekcija
αὐτός αὐτή αὐτό ; on; isti ; grmorf01
καί ; i ; grmorf01
δέ ; a ; grmorf01
οὗτος αὕτη τοῦτο ; ovaj ; grmorf01
τις τι ; netko ; grmorf01
οὐ οὐκ οὐχ ; ne ; grmorf01
εἰς (+ ak.) ; u (smjer) ; grmorf01
ἐπί ; na ; grmorf01
φημί φήσω impf. ἔφην ; govoriti ; grmorf01
λόγος λόγου ὁ ; riječ ; grmorf01
μέγας μεγάλη μέγα ; velik ; grmorf01
λαμβάνω λήψομαι ἔλαβον εἴληφα εἴλημμαι ἐλήφθην ; uzimati ; grmorf01
ἀρχή ἀρχῆς ἡ ; početak vlast ; grmorf01
μᾶλλον ; više radije ; grmorf01
εἰμί ἔσομαι impf. ἦν infin. εἶναι ; biti postojati ; grmorf02
ὅς ἥ ὅ ; koji ; grmorf02
περί ; o oko ; grmorf02
θεός θεοῦ ὁ/ἡ ; bog ; grmorf02
ἄνθρωπος –ου ὁ/ἡ ; čovjek ; grmorf02
γῆ γῆς ἡ ; zemlja ; grmorf02
φαίνω φανῶ ἔφηνα πέφηνα πέφασμαι ἐφάνην ; pokazivati ; grmorf02
δίκαιος δικαία δίκαιον ; pravedan ; grmorf02
ἔχω ἕξω / σχήσω ἔσχον ἔσχηκα impf. εἶχον ; imati ; grmorf03
ὅστις ἥτις ὅ τι ; koji god ; grmorf03
εἰ ; ako ; grmorf03
ποιέω ποιήσω ἐποίησα πεποίηκα πεποίημαι ἐποιήθην ; raditi ; grmorf03
εἷς μία ἕν ; jedan ; grmorf03
καλέω καλῶ ἐκάλεσα κέκληκα κέκλημαι ἐκλήθην ; zvati ; grmorf03
δεῖ δεήσει impf. ἔδει ; treba ; grmorf03
ὥσπερ ; kao što ; grmorf03
μέρος μέρους τό ; dio  ; grmorf03
κατά ; niz kroz po ; grmorf04
ἐκ ἐξ ; iz ; grmorf04
πόλις πόλεως ἡ ; grad ; grmorf04
δύναμις δυνάμεως ἡ ; sila moć ; grmorf04
βασιλεύς βασιλέως ὁ ; kralj ; grmorf04
ἔρχομαι fut. εἶμι or ἐλεύσομαι 2 aor. ἦλθον ἐλήλυθα ; ići ; grmorf04
ὀλίγος ὀλίγη ὀλίγον ; malen ; grmorf04
ναῦς νεώς ἡ ; brod ; grmorf04
βάρβαρος –ον ; negrčki stran  ; grmorf04
ἡγέομαι ἡγήσομαι ἡγησάμην ἥγημαι ; voditi ; grmorf04
ὦ ; o! ; grmorf05
ψυχή ψυχῆς ἡ ; duša ; grmorf05
εὑρίσκω εὑρήσω ηὗρον ili εὗρον ηὕρηκα ili εὕρηκα εὕρημαι εὑρέθην ; pronalaziti ; grmorf05
ἄγω ἄξω ἤγαγον ἦχα ἦγμαι ἤχθην ; voditi ; grmorf05
πρᾶγμα πράγματος τό ; stvar posao ; grmorf05
ζῷον ζῴου τό ; životinja ; grmorf05
ἐνταῦθα ; ovdje tada ; grmorf05
κεῖμαι κείσομαι ; ležati ; grmorf05
ὡς ; da kad ; grmorf06
διά;kroz zbog;grmorf06
ἀλλά;nego;grmorf06
οὐδείς οὐδεμία οὐδέν;nijedan ništa;grmorf06
ἀγαθός –ή –όν;dobar;grmorf06
ἡμέρα ἡμέρας ἡ;dan;grmorf06
καλός –ή –όν;lijep;grmorf06
αἰσθάνομαι αἰσθήσομαι ᾐσθόμην ᾔσθημαι;opaziti spoznati;grmorf06
γάρ;jer naime;grmorf07
γίγνομαι γενήσομαι ἐγενόμην γέγονα γεγένημαι ἐγενήθην;postati roditi se;grmorf07
οὖν;dakle;grmorf07
παρά (+ gen. dat. ak.);uz od;grmorf07
ὁράω ὄψομαι εἶδον ἑόρακα / ἑώρακα ὤφθην impf. ἑώρων;gledati;grmorf07
ὥστε;tako da;grmorf07
ἅπας ἅπασα ἅπαν;svekolik cijeli;grmorf07
παῖς παιδός ὁ/ἡ;dijete rob;grmorf07
δόξα δόξης ἡ;slava misao;grmorf07
νέος νέα νέον;mlad nov;grmorf07
ἄλλος ἄλλη ἄλλο;drugi;grmorf08
μετά (+ gen. ak.);nakon sa;grmorf08
εἶπον;reći;grmorf08
δή;dakle;grmorf08
πρῶτος πρώτη πρῶτον;prvi;grmorf08
πλέω πλεύσομαι ἔπλευσα πέπλευκα πέπλευσμαι ἐπλεύσθην;ploviti;grmorf08
πλῆθος πλήθους τό;mnoštvo;grmorf08
εἶμι infin. ἰέναι ptc. ἰών ἰοῦσα ἰόν;ići ću;grmorf08
καιρός καιροῦ ὁ;pravi čas;grmorf08
κίνδυνος κινδύνου ὁ;opasnost;grmorf08
ἐάν (εἰ-ἄν);ako;grmorf09
οὕτως;tako;grmorf09
ἑαυτοῦ ἑαυτῆς ἑαυτοῦ;sebe;grmorf09
δοκέω δόξω ἔδοξα;smatrati čini se;grmorf09
ἀνήρ ἀνδρός ὁ;muž čovjek;grmorf09
ἐμός ἐμή ἐμόν;moj;grmorf09
αἴτιος αἰτία αἴτιον;kriv;grmorf09
γυνή γυναικός ἡ;žena;grmorf09
ὑπέρ (+ gen. ak.);iznad preko;grmorf09
πατήρ πατρός ὁ;otac;grmorf09
φίλος φίλη φίλον;drag prijateljski;grmorf09
πρός (+ gen. dat. ak.);uz prema;grmorf10
δίδωμι δώσω ἔδωκα δέδωκα δέδομαι ἐδόθην;davati;grmorf10
χράομαι χρήσομαι ἐχρησάμην κέχρημαι ἐχρήσθην;trebati upotrebljavati;grmorf10
μόνον;samo;grmorf10
ὄνομα ὀνόματος τό;ime;grmorf10
ἀκούω ἀκούσομαι ἤκουσα ἀκήκοα ἠκούσθην;slušati;grmorf10
κόσμος κόσμου ὁ;red ukras svijet;grmorf10
πρᾶξις πράξεως ἡ;djelovanje rad;grmorf10
ποιητής –οῦ ὁ;pjesnik;grmorf10
δηλόω δηλώσω ἐδήλωσα δεδήλωκα ἐδηλώθην;pokazati;grmorf10
βούλομαι βουλήσομαι βεβούλημαι ἐβουλήθην;htjeti;grmorf10
τοιοῦτος τοιαύτη τοιοῦτο;takav;grmorf10
πᾶς πᾶσα πᾶν;svaki svi;grmorf11
ὑπό (+ gen. dat. ak.);od pod;grmorf11
ὅσος ὅση ὅσον;kolik;grmorf11
φύω φύσω ἔφυσα;roditi;grmorf11
φέρω οἴσω ἤνεγκα / ἤνεγκον ἐνήνοχα ἐνήνεγμαι ἠνέχθην;nositi;grmorf11
γένος γένους τό;rod pleme vrsta;grmorf11
τόπος τόπου ὁ;mjesto;grmorf11
τότε;tada;grmorf11
ποταμός –οῦ ὁ;rijeka;grmorf11
παρέχω παρέξω παρέσχον παρέσχηκα impf. παρεῖχον;pružati dati;grmorf11
νῆσος νήσου ἡ;otok;grmorf11
πολύς πολλή πολύ;mnogi;grmorf12
κακός –ή –όν;loš;grmorf12
μάλιστα;najviše svakako;grmorf12
πράσσω πράξω ἔπραξα πέπραχα πέπραγμαι ἐπράχθην;raditi;grmorf12
νόμος νόμου ὁ;običaj zakon;grmorf12
σύν (+ dat.);s sa;grmorf12
μανθάνω μαθήσομαι ἔμαθον μεμάθηκα;učiti;grmorf12
ἡδονή –ῆς ἡ;užitak radost;grmorf12
ἄριστος ἀρίστη ἄριστον;najplemenitiji najbolji;grmorf12
ἀλήθεια ἀληθείας ἡ;istina;grmorf12
τιμάω τιμήσω ἐτίμησα τετίμηκα τετίμημαι ἐτιμήθην;častiti;grmorf12
αἰσχρός –ά –όν;ružan sramotan;grmorf12
ἀπό (+ gen.);od;grmorf13
οὔτε ... οὔτε;niti... niti;grmorf13
ἄρχω ἄρξω ἦρξα ἦργμαι ἤρχθην;početi vladati;grmorf13
συμβαίνω συμβήσομαι συνέβην συμβέβηκα;dogoditi se;grmorf13
χρή impf. χρῆν / ἐχρῆν infin. χρῆναι;treba;grmorf13
ὅπως;da (namjerni veznik);grmorf13
σημεῖον σημείου τό;znak;grmorf13
τέλος τέλους τό;svršetak cilj postignuće;grmorf13
νύξ νυκτός ἡ;noć;grmorf13
οὐρανός –οῦ ὁ;nebo;grmorf13
ἔξω (+ gen.);izvan;grmorf13
μή;ne (uz imperativ konjunktiv itd.);grmorf14
ἕτερος ἑτέρα ἕτερον;drugi;grmorf14
σῶμα σώματος τό;tijelo;grmorf14
δύναμαι δυνήσομαι ἐδυνήθην δεδύνημαι;moći;grmorf14
οἶδα infin. εἰδέναι imper. ἴσθι plupf. ᾔδειν;znati;grmorf14
τέχνη τέχνης ἡ;umijeće vještina;grmorf14
κρίνω κρινῶ ἔκρινα κέκρικα κέκριμαι ἐκρίθην;suditi;grmorf14
ἔνθα;tamo;grmorf14
δεσπότης –ου ὁ;gospodar;grmorf14
βλέπω βλέψομαι ἔβλεψα;gledati;grmorf14
γε;baš;grmorf15
μόνος μόνη μόνον;jedini sam;grmorf15
νῦν νυνί;sada;grmorf15
γράφω γράψω ἔγραψα γέγραφα γέγραμμαι ἐγράφην;pisati;grmorf15
τοίνυν;stoga nadalje;grmorf15
ἀφαιρέω ἀφαιρήσω ἀφεῖλον ἀφῄρηκα ἀφῄρημαι ἀφῃρέθην;oduzimati;grmorf15
τύχη τύχης ἡ;slučaj sreća;grmorf15
δέχομαι δέξομαι ἐδεξάμην δέδεγμαι ἐδέχθην;primati;grmorf15
πατρίς πατρίδος ἡ;domovina;grmorf15
ἐκεῖ;tamo;grmorf15
πυνθάνομαι πεύσομαι ἐπυθόμην πέπυσμαι;pitati čuti saznati;grmorf15
ἐγώ ἐμοῦ (pl.) ἡμεῖς ἡμῶν;ja mi;grmorf16
ὅλος ὅλη ὅλον;cijeli;grmorf16
πάσχω πείσομαι ἔπαθον πέπονθα;trpjeti;grmorf16
νομίζω νομιῶ ἐνόμισα νενόμικα νενόμισμαι ἐνομίσθην;smatrati običavati;grmorf16
ἀδελφός –οῦ ὁ;brat;grmorf16
πολλάκις;često;grmorf16
χρῆμα χρήματος τό;novac imovina stvar;grmorf16
ἕνεκα (iza genitiva);radi (prijedlog);grmorf16
πάνυ;posve;grmorf16
δεινός –ή –όν;strašan;grmorf16
οἶκος οἴκου ὁ;kuća;grmorf16
ἕκαστος ἑκάστη ἕκαστον;svaki;grmorf17
ἵνα;da (namjerni veznik);grmorf17
ἅμα (+ dat.);u isto vrijeme (prilog) zajedno s (prijedlog);grmorf17
μηδέ;i ne;grmorf17
ἥλιος ἡλίου ὁ;sunce;grmorf17
ἔοικα ptc. εἰκώς;nalikovati činiti se;grmorf17
δίκη δίκης ἡ;pravda parnica suđenje;grmorf17
νικάω νικήσω ἐνίκησα νενίκηκα νενίκημαι ἐνικήθην;pobijediti;grmorf17
λανθάνω λήσω ἔλαθον λέληθα;biti sakriven;grmorf17
αἰτέω αἰτήσω ᾔτησα ᾔτηκα ᾔτημαι ᾐτήθην;tražiti moliti;grmorf17
ἐκεῖνος ἐκείνη ἐκεῖνον;onaj;grmorf18
εἶτα;poslije;grmorf18
πόλεμος πολέμου ὁ;rat;grmorf18
ἀμφότερος ἀμφοτέρα ἀμφότερον;oba;grmorf18
ἀποδίδωμι ἀποδώσω ἀπέδωκα ἀποδέδωκα ἀποδέδομαι ἀπεδόθην;vratiti prodati;grmorf18
ἀναιρέω ἀναιρήσω ἀνεῖλον ἀνῄρηκα ἀνῄρημαι ἀνῃρέθην;dići;grmorf18
ἥκω ἥξω pf. ἧκα;došao sam ovdje sam;grmorf18
διαφορά –ᾶς ἡ;razlika;grmorf18
φανερός –ά –όν;jasan;grmorf18
παλαιός –ά –όν;star;grmorf18
οὐδέ;i ne;grmorf19
οἴομαι / οἶμαι οἰήσομαι impf. ᾤμην aor. ᾠήθην;misliti;grmorf19
σαφής σαφές;jasan;grmorf19
οἰκία οἰκίας ἡ;kuća;grmorf19
ἐάω ἐάσω εἴασα;dopustiti;grmorf19
πειράω / πειράομαι πειράσομαι ἐπείρασα πεπείραμαι ἐπειράθην;pokušati iskušati;grmorf19
κτάομαι κτήσομαι ἐκτησάμην κέκτημαι;stjecati;grmorf19
μάλα;vrlo;grmorf19
ἐν (+ dat.);u;grmorf20
γιγνώσκω γνώσομαι ἔγνων ἔγνωκα ἔγνωσμαι ἐγνώσθην;spoznati;grmorf20
κοινός –ή –όν;zajednički;grmorf20
ἀληθής –ές;istinit;grmorf20
πέμπω πέμψω ἔπεμψα πέπομφα πέπεμμαι ἐπέμφθην;slati;grmorf20
στρατηγός –οῦ ὁ;vojskovođa;grmorf20
πρέσβυς πρέσβεως ὁ;starac poslanik;grmorf20
μέλλω μελλήσω ἐμέλλησα;namjeravati;grmorf20
