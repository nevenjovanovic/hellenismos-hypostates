# README for hellenismos-hypostates - support scripts and data for an Ancient Greek reader #

The *hellenismos-hypostates* repository contains XQuery scripts and XML (and some CSV) files supporting the Ancient Greek reader, 60 brief extracts of Greek prose, prepared for the course Grčka morfologija 1 (Greek morphology 1), 2018/19, University of Zagreb, Croatia, Faculty of Humanities and Social Sciences, [Department of Classical Philology](http://www.ffzg.unizg.hr/klafil/). The reader, typeset with LaTeX, with vocabulary exercises prepared for Anki, has its own repository, [hellenismos](https://bitbucket.org/nevenjovanovic/hellenismos).

The XML files in this repository contain morphological annotation and lemmatization of Greek words, a list of "core vocabulary" (most common words in Ancient Greek), based on the [Dickinson College Greek Core Vocabulary](http://dcc.dickinson.edu/vocab/core-vocabulary) lists (with Croatian translations), and lists of words used in the reader and in the exercises.

The XQuery scripts select specific data from several XML databases (of reading selections, morphological annotations and lemmatizations, vocabulary lists).

This repository was prepared by [Neven Jovanović](http://orcid.org/0000-0002-9119-399X).

# License #

[CC Attribution 4.0 International](https://bitbucket.org/nevenjovanovic/hellenismos-hypostates/src/master/LICENSE.md)

# Contents #

Directory structure, with some explanations:

```bash
.
├── data (full texts from which selections are taken, from the Open Greek and Latin project)
├── izbor_pos_aldt3 (selections, machine-annotated in ALDT scheme)
├── izbor_pos_ud (selections, machine-annotated in UD scheme)
├── popis_tekstova (lists of titles and CTS URIs for the reader)
├── pos_perseids (selections annotated by humans in Perseids, ALDT scheme)
├── pos_ud (full texts, machine-annotated, UD scheme)
├── restlovi (various working files and directories)
│   ├── aldt_proba (a test version of ALDT files)
│   │   ├── izbor_pos_aldt
│   │   └── izbor_pos_aldt2
│   ├── export (annotated files imported from Perseids)
│   ├── izbor_pos_aldt2
├── rijeci (vocabulary)
│   ├── GrcLatCore (DCC Greek and Latin Core lists, here in XML)
│   └── pomocno (accessory lists for vocabulary control)
└── scripts (XQuery files)

```

# Databases to create #

TBA

# Workflows #

## Annotate a new selection ##

## Add a new annotated text from Perseids ##

## Develop new set of vocabulary exercises ##

## Update vocabulary lists ##

# Who do I talk to? #

* Repo owner: [Neven Jovanović](https://bitbucket.org/nevenjovanovic)

